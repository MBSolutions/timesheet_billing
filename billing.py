# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    timesheet_line = fields.Many2One('timesheet.line', 'Timesheet Line',
            ondelete='CASCADE', readonly=True)

BillingLine()
